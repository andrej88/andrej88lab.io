---
layout:            post
title:             Introducing Stratum
date:              2018-02-16
categories:        stratum
published:         false
excerpt_separator: <!-- EXCERPT -->
---

# Background

For a very long time, I have had a desire to make a platforming video game. As a child, I'd dream up what I considered amazing video game ideas and draw levels that would fit in a Mario game. Around the age of eleven or twelve, I fiddled around with Game Maker and stitched together some of the bundled sprites to create a very rudimentary platformer (now lost to the bytes of time). Then, almost three years ago, I decided to dip my toes in the world of game development and fiddled around a bit with Unity and the Godot Engine.

While I had grown a great deal, my desire to one day create a video game had remained steadfast. Platformers are what I grew up with and still enjoy playing. The main thing stopping me was a lack of ideas. What kind of world should I create? What will the gameplay be like? What about the story? I was quite wary of falling into the trap of believing all my ideas were great (perhaps too much, perhaps not enough), so I really wanted something that felt *perfect*.

<!-- EXCERPT -->

Just over two years ago I woke up with a particularly fun dream lingering in my memory. I had been gliding through a deep and narrow gorge, above a rushing river and beneath a bright blue sky. I passed a set of cascading waterfalls, and the gorge transitioned into a long underground cavern, passing a colorful palette of different "biomes" — crystalline caverns, volcanic chambers, and icy chasms.

Once the initial, still half-asleep "Wow!" phase faded away, I started mulling this idea over a bit with a more reasonable (and awake) mindset. I quite liked this kind of world, and it felt inherently *video-gamey*, for lack of a better phrase. I tried to refine the idea out a bit more and decided that, while not the *perfect* idea I had wished for, a colorful and labyrinthine cave system was a great setting for a platformer video game. I resigned to let the kinks iron themselves out evolve over time.

Shortly thereafter, in early January of 2016, I dived into LibGDX and began programming what I now call *Stratum*. Two years later, and... progress doesn't look spectacular. It's still a skeleton of a foundation of a game.

![Stratum says "Hello, World!"]({{ "/assets/stratum 1 introduction/Hello World.png" | relative_url }})

In my defense, I had put the project on hold after about half a year of work. I had invested a significant amount of time in the project, and I was becoming somewhat burned out from coding back-end low-level functionality: platformer physics, an entity/component system, tilemap processing, and so forth. I also felt I was at a bit of an impasse from the game design point of view. By this point, I had fleshed out the setting quite a bit and experimented with some potential storylines. I had pages of ideas and (very amateurish) concept art in a dedicated notebook. Despite all this, the most important component was still missing: I had no idea what the *gameplay* would be like.

Fast-forward to December of 2017 — almost a year and a half later. While I never gave up on Stratum, I always intended to continue when I wasn't distracted by other pet projects or if inspiration decided to strike. And strike it did. I rode the wave of motivation and, after some repository housekeeping, I took a look at my old code and tried to figure out what was going on. My programming skills have grown considerably since I had last touched Stratum, so I got to work on cleaning up some of the old code and restructuring the program a bit.

# The Road Ahead

This unexpected motivation would surely have died away in a matter of days had I not also arrived at a solution to the problem that had been looming over my shoulder the last time I had worked on Stratum. I finally had an idea for the gameplay itself, and it felt like a tremendous hurdle had been conquered.

And now, in the here and now, I finally have a concrete idea of what the game will look like and how it will play.

The game will be a semi-open-world adventure-platformer. The setting is a sprawling underground cave system that you discover and explore throughout your adventure. As the story progresses, more of the world is revealed, already-discovered areas change and evolve, and the player gains additional abilities.

The player's character is equipped with a pair of special gloves, and I hope that this mechanic makes Stratum more than just a basic platformer. The primary purpose of the gloves is to dig through rock and attack enemies, and I'm sticking with the idea because I believe it has quite a bit of potential. For example, the player may encounter burrowing gloves that allow one to zoom through the ground, cleat-gloves that allow wall climbing, or heat-resistant gloves for digging into ice or hot rock.

As I continue work on Stratum, the details are sure to evolve and change, as they have already done over the two years this game has spent simmering in my mind. The most important thing for me is that I stumbled upon an idea that I think is fun and that feels like it has great potential. Now the real work can begin.

What stage is the game at right now? There are a few elements that I can confidently say are *done*. I have the platformer physics all written up. There's a flexible system for adding whatever in-world objects I may come up with. The code can process a Tiled .tmx tilemap and have it show up as a level. I have a completed world map infrastructure. And I have done work on a number of mechanics that are fundamental to a game like this — moving platforms, enemies, and doors, for example.

There's a lot of progress to be made on both the game design and programming fronts, but progress is steady and the future is bright.

For now, I think this serves as an appropriate introduction for the project I'm calling Stratum. In a future post, I will discuss some of the inspirations behind Stratum and the technical details of the project. As I make further progress, I'll keep this blog updated with stories, screenshots, and more.

I expect it will be a long time before anyone reads this, but the common opinion seems to be that the best time to start a dev blog is "now". Regardless, thank you for reading and I hope this post was of interest to you!