---
layout:            post
title:             Template   # CHANGE ME
date:              2018-02-12 # CHANGE ME
categories:        stratum
published:         false      # DELETE ME
excerpt_separator: <!-- EXCERPT -->
---

![This is how you embed an image]({{ "/assets/stratum template post/jupiter pixel.png" | relative_url }})

Excerpt.

<!-- EXCERPT -->

Post-excerpt. A [link][jekyll-docs].

```kotlin
fun this(is: Kotlin) {
    code
}
```

# Header 1

text

## Header 2

text

### Header 3

text

#### Header 4

text

[jekyll-docs]: http://jekyllrb.com/docs/home
